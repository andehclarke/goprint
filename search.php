<?php 
/**
 * The template for displaying search results pages
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 */
 	
get_header(); ?>
			
	<div class="content">

		<div class="inner-content grid-x grid-margin-x grid-padding-x">
	
			<main class="main cell small-offset-1 small-10 medium-offset-2 medium-8 grid-x" role="main">

				<div id="blur-bg" class="cell small-12"></div>

				<div id="blur-overlay" class="cell small-12 grid-x">
					
					<header>
						<h1 class="archive-title"><?php _e( 'Search Results for:', 'jointswp' ); ?> <?php echo esc_attr(get_search_query()); ?></h1>
					</header>

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
						<!-- To see additional archive styles, visit the /parts directory -->
						<?php get_template_part( 'parts/loop', 'archive' ); ?>
						
					<?php endwhile; ?>	

						<?php joints_page_navi(); ?>
						
					<?php else : ?>
					
						<?php get_template_part( 'parts/content', 'missing' ); ?>
							
					<?php endif; ?>
					
				</div>
		
		    </main> <!-- end #main -->
		
		    <?php // get_sidebar(); ?>
		
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>

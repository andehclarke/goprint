<?php
/**
 * The homepage template file
 */

get_header(); ?>
			
	<div class="content">
	
		<div class="inner-content grid-x grid-margin-x grid-padding-x">

		    <main class="main cell small-offset-1 small-10 medium-offset-2 medium-8 grid-x" role="main" id="home-main-content">

				<div id="blur-bg" class="cell small-12"></div>

				<div id="blur-overlay" class="cell small-12 grid-x">
					<div class="full-height cell small-12 center-all">
						<div id="main-header">
							<h1><span class="gp-printing">Printing</span><br />&amp;<br /><span class="gp-embroidery">Embroidery</span></h1>
						</div>
						<div class="cell small-12">
							<?php dynamic_sidebar( 'home-intro' ); ?>
						</div>
					</div>

					<div class="cell small-12 grid-x" id="brand-logos">
						<div class="small-12 cell">
							<h3>Supply Brands</h3>
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/01.jpg" />
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/02.jpg" />
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/03.jpg" />
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/04.jpg" />
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/05.jpg" />
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/06.jpg" />
						</div>
						
						
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/07.jpg" />
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/08.jpg" />
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/09.jpg" />
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/10.jpg" />
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/11.jpg" />
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/12.jpg" />
						</div>

						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/13.jpg" />
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/14.jpg" />
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/15.jpg" />
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/16.jpg" />
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/17.jpg" />
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/18.jpg" />
						</div>
						
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/19.jpg" />
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/20.jpg" />
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/21.jpg" />
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/22.jpg" />
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/23.jpg" />
						</div>
						<div class="small-3 medium-2 cell">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logos/24.jpg" />
						</div>


					</div>
				</div>
																								
		    </main> <!-- end #main -->
		    
		    <?php //get_sidebar(); ?>

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>
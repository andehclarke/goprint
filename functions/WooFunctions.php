<?php
    require_once(get_template_directory().'/functions/WooHooks.php');
    function WooCategories() {
        $taxonomy     = 'product_cat';
        $orderby      = 'name';  
        $show_count   = 0;      // 1 for yes, 0 for no
        $pad_counts   = 0;      // 1 for yes, 0 for no
        $hierarchical = 1;      // 1 for yes, 0 for no  
        $title        = '';  
        $empty        = 0;
        $args = array(
            'taxonomy'     => $taxonomy,
            'orderby'      => $orderby,
            'show_count'   => $show_count,
            'pad_counts'   => $pad_counts,
            'hierarchical' => $hierarchical,
            'title_li'     => $title,
            'hide_empty'   => $empty
        );
        $all_categories = get_categories( $args );
        foreach ($all_categories as $cat) {
            if($cat->category_parent == 0) {
                $category_id = $cat->term_id; 
                $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
                $image = wp_get_attachment_image( $thumbnail_id ); 
                echo '<div class="cell small-12 medium-6 large-4 cat-cards">';
                echo '<a href="'. get_term_link($cat->slug, 'product_cat') .'">';
                echo '<div class="card center-all">';
                echo '<div class="card-section"><div class="oct">';
                echo $image;
                echo '</div>';
                echo '<h2>'. $cat->name .'</h2>';
                echo '<p>'.$cat->description.'</p>';
                echo '<button class="button large">Take a look</button>';
                echo '</div></div></a></div>';
            }       
        }
    }
    // Alter shop columns
    function wpex_woo_shop_columns( $columns ) {
        return 3;
    }

    if ( ! function_exists( 'woocommerce_get_product_thumbnail' ) ) {

        /**
         * Get the product thumbnail, or the placeholder if not set.
         *
         * @subpackage	Loop
         * @param string $size (custom: 'thumbnail').
         * @param int    $deprecated1 Deprecated since WooCommerce 2.0 (default: 0).
         * @param int    $deprecated2 Deprecated since WooCommerce 2.0 (default: 0).
         * @return string
         */
        function woocommerce_get_product_thumbnail( $size = 'thumbnail', $deprecated1 = 0, $deprecated2 = 0 ) {
            global $product;
    
            $image_size = apply_filters( 'single_product_archive_thumbnail_size', $size );
    
            return $product ? $product->get_image( $image_size ) : '';
        }
    }

    add_action( 'gpwc_archive_image', 'gpwc_category_image', 2 );
    add_action( 'gpwc_archive_description', 'gpwc_taxonomy_archive_description', 10 );
    add_action( 'gpwc_archive_description', 'gpwc_product_archive_description', 10 );

<?php 
/**
 * The template for displaying contact page
 *
 */

contact_email();

get_header(); ?>
			
	<div class="content">
	
		<div class="inner-content grid-x grid-margin-x grid-padding-x">
	
		    <main class="main cell small-offset-1 small-10 medium-offset-2 medium-8 grid-x" role="main">

				<div id="blur-bg" class="cell small-12"></div>

				<div id="blur-overlay" class="cell small-12 grid-x">
				
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<?php get_template_part( 'parts/loop', 'page-contact' ); ?>
						
					<?php endwhile; endif; ?>	
					
				</div>

			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->
	
	</div> <!-- end #content -->

<?php get_footer(); ?>
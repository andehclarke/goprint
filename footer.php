<?php
/**
 * The template for displaying the footer. 
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */			
 ?>
					
				<footer class="footer" role="contentinfo">
					
					<div class="inner-footer grid-x grid-margin-x grid-padding-x">
						
						<div class="small-4 medium-4 cell">
							<nav role="navigation">
	    						<?php joints_footer_links(); ?>
	    					</nav>
						</div>
						
						<div class="small-4 medium-4 cell center-align">
							<p class="social-media">
								<a href="https://www.facebook.com/goprintnuneaton/" target="_blank"><span class="socicon-facebook"></span></a> <a href="https://twitter.com/goprintnuneaton" target="_blank"><span class="socicon-twitter"></span></a>
							</p>
						</div>
						
						<div class="small-4 medium-4 cell text-right">
							<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.</p>
							<p class="credits">Designed by <a target="_BLANK" href="http://www.andyclarke.website">Andy Clarke</a></p>
						</div>
					
					</div> <!-- end #inner-footer -->
				
				</footer> <!-- end .footer -->
			
			</div>  <!-- end .off-canvas-content -->
					
		</div> <!-- end .off-canvas-wrapper -->
		
		<?php wp_footer(); ?>
		
	</body>
	
</html> <!-- end page -->
<?php
/**
 * Template part for displaying page content in page.php
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('small-12'); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
	<?php $result = get_extended( get_post_field( 'post_content', get_the_ID() ) ); ?>
	<header class="article-header">
		<h1 class="page-title"><?php the_title(); ?></h1>
	</header> <!-- end article header -->
					
    <section class="entry-content grid-x" itemprop="articleBody">
		<div class="small-10 small-offset-1 table-scroll">
			<?php the_content('', TRUE); ?>
		</div>
	</section> <!-- end article section -->
						
	<footer class="article-footer">
		 <?php wp_link_pages(); ?>
	</footer> <!-- end article footer -->
						    
	<?php comments_template(); ?>
					
</article> <!-- end article -->
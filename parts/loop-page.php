<?php
/**
 * Template part for displaying page content in page.php
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
	<?php $result = get_extended( get_post_field( 'post_content', get_the_ID() ) ); ?>
	<header class="article-header">
		<h1 class="page-title"><?php the_title(); ?></h1>
		<h3 class="page-excerpt"><?php echo $result['main']; ?></h3>
	</header> <!-- end article header -->
					
    <section class="entry-content grid-x grid-margin-x grid-padding-x" itemprop="articleBody">
		<div class="cell small-12 medium-4 center-align">
			<p><?php the_post_thumbnail('medium'); ?></p>
			<p><a href="/goprint/range" class="button large expanded">View our range</a></p>
		</div>
		<div class="cell small-12 medium-8">
			<?php the_content('', TRUE); ?>
		</div>
	</section> <!-- end article section -->
						
	<footer class="article-footer">
		 <?php wp_link_pages(); ?>
	</footer> <!-- end article footer -->
						    
	<?php comments_template(); ?>
					
</article> <!-- end article -->
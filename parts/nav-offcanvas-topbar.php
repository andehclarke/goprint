<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>

<div data-sticky-container>
	<div class="top-bar sticky small-12 grid-x" id="top-bar-menu" data-sticky data-margin-top="0" data-options="stickyOn:small;">
		<div class="show-for-small-only small-12">
			<ul class="menu">
				<li><a href="<?php echo home_url(); ?>"><img id="gp-logo-sm" src="<?php echo get_template_directory_uri(); ?>/assets/images/menu-logo.png" /></a></li>
				<li><button id="menu-button" class="button large" type="button" data-toggle="off-canvas">Menu</button></li>
			</ul>
		</div>
		<div class="medium-12 show-for-medium grid-x">
			<div class="medium-offset-2 medium-3 gp-menu-left">
				<?php joints_topleft_nav(); ?>
			</div>
			<div class="medium-2 center-all">
				<a href="<?php echo home_url(); ?>"><img id="gp-logo" src="<?php echo get_template_directory_uri(); ?>/assets/images/menu-logo.png" /></a>
			</div>
			<div class="medium-3 gp-menu-right">
				<?php joints_topright_nav(); ?>
			</div>
			<div class="medium-2 gp-menu-address">
				<?php dynamic_sidebar( 'menu-info' ); ?>
			</div>
		</div>
	</div>
</div>
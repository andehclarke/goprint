<?php
/**
 * Template part for displaying page content in page.php
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
	<?php $result = get_extended( get_post_field( 'post_content', get_the_ID() ) ); ?>
	<header class="article-header">
		<h1 class="page-title"><?php the_title(); ?></h1>
		<h3 class="page-excerpt"><?php echo $result['main']; ?></h3>
	</header> <!-- end article header -->
					
    <section class="entry-content grid-x grid-margin-x grid-padding-x text-center" itemprop="articleBody">
		<div class="cell small-12 medium-3">
			<?php the_content('', TRUE); ?>
		</div>
		<div class="cell small-12 medium-9">
			<iframe style="border: 0;" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJlevzjSROd0gRw1yuBFGfhJo&amp;key=AIzaSyBF43Ekv8aPb-C0tqixABI5dPqUH_xMJQQ" width="100%" height="400" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
		</div>
		<div class="cell small-12">
			<p><?php the_post_thumbnail('medium'); ?></p>
			<form data-abide novalidate action="" method="POST" id="contact">
				<div data-abide-error class="alert callout" style="display: none;">
					<p><i class="fi-alert"></i> There are some errors in your form.</p>
				</div>
				<div class="grid-x grid-padding-x center-all-m">
					<div class="cell small-12 medium-6">
						<label>
							<h2>Your name</h2>
							<input type="text" name="cf_name" placeholder="Your Name" required pattern="text" />
							<span class="form-error">
							Let me know who you are...
							</span>
						</label>
					</div>
					<div class="cell small-12 medium-6">
						<label>
							<h2>Your email</h2>
							<input type="email" name="cf_email" placeholder="example@email.com" required pattern="email" />
							<span class="form-error">
							Who do I reply to?
							</span>
						</label>
					</div>
					<div class="cell small-12">
						<label>
							<h2>Your message</h2>
							<textarea rows="7" name="cf_message" placeholder="What you've got to share"></textarea>
						</label>
					</div>
					<div class="cell small-12 submit">
						<input class="hide-me" name="anti-spam" type="text" />
						<button class="button large" type="submit" value="Send">Send</button>
					</div>
				</div>
			</form>
		</div>
	</section> <!-- end article section -->
						
	<footer class="article-footer">
		 <?php wp_link_pages(); ?>
	</footer> <!-- end article footer -->
						    
	<?php comments_template(); ?>
					
</article> <!-- end article -->